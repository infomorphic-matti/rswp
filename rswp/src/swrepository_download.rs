//! Stuff for FFU downloading and searching for methods and such.
//!
//! Microsoft has a REST API at `https://api.swrepository.com/rest-api/discovery/1/package`
//! which can be searched for firmware and other files relating to various devices
//! (some being lumia stuff).
//!
//! This contains both raw APIs and wrappers for the lumia stuff we actually want to do.
//!
//! See `LumiaDownloadModel.cs` in `WPinternals` for details on this.

/// Raw JSON API stuff
pub mod raw {
    /// Package discovery
    pub mod package {
        use std::borrow::Cow;

        use chain_trans::Trans;
        use http::Request;
        use serde::{Deserialize, Serialize};
        use smallvec::smallvec;

        #[derive(PartialEq, Eq, Clone, Serialize, Deserialize, Debug)]
        #[serde(from = "String", into = "String")]
        pub enum PackageType {
            Firmware,
            Unknown(String),
        }

        impl From<String> for PackageType {
            fn from(value: String) -> Self {
                Self::from(value.as_str())
            }
        }

        impl<'a> From<&'a str> for PackageType {
            fn from(value: &'a str) -> Self {
                match value {
                    "Firmware" => Self::Firmware,
                    other => Self::Unknown(other.to_owned()),
                }
            }
        }

        impl From<PackageType> for String {
            fn from(value: PackageType) -> Self {
                <PackageType as Into<Cow<'static, str>>>::into(value).into_owned()
            }
        }

        impl<'a> From<PackageType> for Cow<'a, str> {
            fn from(value: PackageType) -> Self {
                match value {
                    PackageType::Firmware => Cow::Borrowed("Firmware"),
                    PackageType::Unknown(v) => Cow::Owned(v),
                }
            }
        }

        #[derive(Copy, Clone, Serialize, PartialEq, Eq, Debug)]
        /// Allow selecting which entries to get.
        pub enum DiscoveryCondition {
            #[serde(rename = "all")]
            All,
            #[serde(rename = "latest")]
            Latest,
            #[serde(rename = "default")]
            Default,
        }

        impl Default for DiscoveryCondition {
            fn default() -> Self {
                Self::Default
            }
        }

        /// URL for finding packages.
        pub const PACKAGE_DISCOVERY_URL: &str =
            "https://api.swrepository.com/rest-api/discovery/1/package";

        pub const API_VERSION: &str = "1";

        /// Json request to the package discovery API
        #[derive(Serialize, Clone, Debug)]
        pub struct PackageQueryJson<'a, Query: Serialize> {
            #[serde(rename = "api-version")]
            api_version: &'a str,
            #[serde(rename = "condition")]
            condition: smallvec::SmallVec<[DiscoveryCondition; 4]>,
            #[serde(rename = "query")]
            query: Query,
        }

        impl<'a, Query: Serialize> PackageQueryJson<'a, Query> {
            pub fn with_query(q: Query) -> Self {
                Self::with_query_and_condition(q, DiscoveryCondition::default())
            }

            pub fn with_query_and_condition(q: Query, condition: DiscoveryCondition) -> Self {
                Self {
                    api_version: API_VERSION,
                    condition: smallvec![condition],
                    query: q,
                }
            }

            pub fn construct_http_query(self) -> Result<Request<Vec<u8>>, serde_json::Error> {
                use http::header::*;
                let raw_bytes = serde_json::to_vec(&self)?;
                Request::post(PACKAGE_DISCOVERY_URL)
                    .header(ACCEPT, "application/json")
                    .header(CONTENT_TYPE, "application/json")
                    .header(USER_AGENT, "SoftwareRepository")
                    .body(raw_bytes)
                    .expect("No reason for invalidity")
                    .trans(Ok)
            }
        }

        /// The software package stuff has a large and complex response, which we only care about
        /// some of (over time, we will likely see a shifting of the API to add more components).
        ///
        /// this provides tools for handling it.
        pub mod response {
            use std::collections::HashMap;

            use chain_trans::Trans;
            use http::uri::{Authority, Scheme};
            use serde::Deserialize;
            use smallvec::SmallVec;

            use crate::swrepository_download::lumia::ProductType;

            use self::files::FileInformationEntry;

            pub mod checksum {
                use std::borrow::Cow;

                use serde::{Deserialize, Serialize};

                #[derive(Deserialize, Serialize, Clone, Eq, PartialEq, Hash, Debug)]
                #[serde(from = "String", into = "Cow<'static, str>")]
                pub enum FileChecksumType {
                    MD5,
                    Unknown(String),
                }

                impl From<String> for FileChecksumType {
                    fn from(value: String) -> Self {
                        value.as_str().into()
                    }
                }

                impl<'a> From<&'a str> for FileChecksumType {
                    fn from(value: &'a str) -> Self {
                        match value {
                            "MD5" => Self::MD5,
                            _ => Self::Unknown(value.to_owned()),
                        }
                    }
                }

                impl<'a> From<FileChecksumType> for Cow<'a, str> {
                    fn from(value: FileChecksumType) -> Self {
                        match value {
                            FileChecksumType::MD5 => "MD5".into(),
                            FileChecksumType::Unknown(v) => v.into(),
                        }
                    }
                }

                /// Note that we don't use serde's tagged enums here because of the lack of neat
                /// "unknown" capabilities.
                #[derive(Serialize, Deserialize, Clone, Hash, Debug)]
                pub struct FileChecksum {
                    #[serde(rename = "type")]
                    checksum_type: FileChecksumType,
                    #[serde(rename = "value")]
                    checksum_value: Option<String>,
                }
            }

            /// Contains file information
            pub mod files {
                use serde::{Deserialize, Serialize};
                use smallvec::SmallVec;

                use super::checksum::FileChecksum;

                /// Single bit of file information.
                #[derive(Deserialize, Serialize, Clone, Hash, Debug)]
                pub struct FileInformationEntry {
                    /// We need one of these! Error if none present (not `Option`
                    #[serde(rename = "fileName")]
                    pub file_name: String,

                    /// File size. Might not always be available so we allow for that.
                    ///
                    /// Not usize in case of large files on 32 bit systems
                    #[serde(rename = "fileSize")]
                    pub file_size: Option<u64>,

                    /// Checksums are usually very few (just one really)
                    #[serde(rename = "checksum", default = "Default::default")]
                    pub checksums: SmallVec<[FileChecksum; 3]>,

                    /// Filetype listed by the API. This has no intrinsic connection to the file
                    /// extension, and often describes it's purpose. Not used very much.
                    #[serde(rename = "fileType")]
                    pub file_type: Option<String>,
                }

                impl FileInformationEntry {
                    pub fn get_name(&self) -> &str {
                        &self.file_name
                    }
                }
            }

            #[derive(Clone, Debug, Deserialize)]
            pub struct SoftwarePackages {
                #[serde(rename = "softwarePackages")]
                pub packages: SmallVec<[SoftwarePackage; 10]>,
            }

            impl SoftwarePackages {
                /// Attempt to obtain the first software package, if it exists
                pub fn get_first_package(&self) -> Option<&SoftwarePackage> {
                    self.packages.get(0)
                }

                /// Attempt to convert into the first software package if it exists.
                pub fn into_first_package(mut self) -> Option<SoftwarePackage> {
                    self.packages.drain(..).next()
                }
            }

            /// The package discovery API returns a list of these things
            #[derive(Clone, Debug, Deserialize)]
            pub struct SoftwarePackage {
                #[serde(rename = "id")]
                pub id: String,

                #[serde(rename = "packageClass")]
                pub package_classes: SmallVec<[String; 5]>,

                #[serde(rename = "manufacturerModelName")]
                /// Likely to just be one.
                pub model_names: SmallVec<[String; 5]>,

                #[serde(rename = "manufacturerVariantName")]
                /// Likely to just be one
                pub variant_names: SmallVec<[String; 5]>,

                #[serde(rename = "manufacturerHardwareModel")]
                /// Likely to just be one product type.
                ///
                /// THIS IS WHAT YOU WANT FOR PATCHING - used by WPinternals
                pub product_types: SmallVec<[ProductType; 5]>,

                /// Likely to just be one - this is the hardware variant - looks a bit like
                /// "059X4V4" (in example response)
                ///
                /// THIS IS WHAT YOU WANT WITH PATCHING - used by WPinternals
                #[serde(rename = "manufacturerHardwareVariant")]
                pub product_codes: SmallVec<[String; 5]>,

                #[serde(rename = "manufacturerProductLine")]
                /// Product line returned from
                pub product_line: Option<String>,

                #[serde(rename = "manufacturerName")]
                /// Just the boring old name of the manufacturer
                pub manufacturer_name: Option<String>,

                #[serde(rename = "files")]
                pub files: Vec<FileInformationEntry>,

                /// Other stuff
                #[serde(flatten, default = "Default::default")]
                pub other_json_fields: HashMap<String, serde_json::Value>,
            }

            impl SoftwarePackage {
                /// The primary product type/hardware model name (first in list). This *should* be
                /// present, but if not we return [`None`].
                ///
                /// It is usually the case that this embeds extra info over the top of whatever was
                /// used to request the response. So if this is [`Some`], it is better to replace
                /// your current less specific [`ProductType`] used to make the request, with this
                /// more precise value.
                ///
                /// This uses the actual listed hardware model.
                pub fn primary_product_type(&self) -> Option<&ProductType> {
                    self.product_types.get(0)
                }

                /// The primary product code/hardware variant identifier (first in list). This *should* be
                /// present, but if not return [`None`]. It is likely this will eventually be
                /// turned into an enum like [`ProductType`], but it is harder to find these
                /// hardware variants than `RM-*` product types.
                pub fn primary_product_code(&self) -> Option<&str> {
                    self.product_codes.get(0).map(AsRef::as_ref)
                }

                /// Package identifier.
                pub fn id(&self) -> &str {
                    &self.id
                }

                /// Get the files that were listed by this API
                pub fn files(&self) -> &[FileInformationEntry] {
                    &self.files
                }

                /// Find the first firmware update file entry (if it exists, and using the same
                /// method as WPinternals, that is, searching for the file extension).
                ///
                /// If you want to do fancy iterator things, check out [`with_first_ffu`].
                pub fn first_ffu_file(&self) -> Option<&FileInformationEntry> {
                    with_first_ffu(self.files.iter(), |a| *a, |a| a)
                }

                /// Get the URI to retrieve information for the given file from the file discovery
                /// REST API.
                ///
                /// See [`crate::swrepository_download::raw::file`] for more info on this.
                pub fn file_uris(
                    &self,
                ) -> impl Iterator<Item = (&FileInformationEntry, Result<http::Uri, http::Error>)> + '_
                {
                    self.files.iter().map(|entry| {
                        let package_id = &self.id;
                        let file_name = &entry.file_name;
                        http::uri::Builder::new()
                            .scheme(Scheme::HTTPS)
                            .authority(Authority::from_static("api.swrepository.com"))
                            // While I'd prefer to build this at the highest granularity,
                            // `http` does not have the API for this so we have to resort to crude `format!`
                            .path_and_query(format!(
                                "/rest-api/discovery/fileurl/1/{package_id}/{file_name}"
                            ))
                            .build()
                            .trans(|query_uri| (entry, query_uri))
                    })
                }

                /// Get the actual HTTP request to retrieve URL details about the files listed in
                /// this response.
                ///
                /// Uses [`Self::file_uris`]
                pub fn file_requests(
                    &self,
                ) -> impl Iterator<
                    Item = (
                        &FileInformationEntry,
                        Result<http::Request<()>, http::Error>,
                    ),
                > + '_ {
                    self.file_uris().map(|(file_entry, uri)| {
                        (
                            file_entry,
                            uri.map(http::Request::get).and_then(|a| a.body(())),
                        )
                    })
                }
            }

            pub const FIRMWARE_FILE_ENDING: &str = ".ffu";

            /// As long as you can extract a file entry from the iterator value, this will let you
            /// select the value for first FFU file (and then apply a map to that).
            pub fn with_first_ffu<I: Iterator, F>(
                mut generator: I,
                mut get_file_entry: impl FnMut(&I::Item) -> &FileInformationEntry,
                on_first_ffu: impl FnOnce(I::Item) -> F,
            ) -> Option<F> {
                generator
                    .find(|item| {
                        get_file_entry(item)
                            .file_name
                            .ends_with(FIRMWARE_FILE_ENDING)
                    })
                    .map(on_first_ffu)
            }

            #[cfg(test)]
            mod test_package_discovery_response_parsing {
                use super::{files::FileInformationEntry, SoftwarePackages};
                /// Example response from the package discovery API
                const EXAMPLE_RESPONSE: &str =
                    include_str!("example-package-discovery-response.json");

                #[test]
                fn ffu_files() {
                    let parsed_response: SoftwarePackages =
                        serde_json::from_str(EXAMPLE_RESPONSE).unwrap();
                    let parsed_response = parsed_response.into_first_package().unwrap();

                    assert_eq!(
                        parsed_response
                            .first_ffu_file()
                            .map(FileInformationEntry::get_name),
                        Some("RM1085_1078.0053.10586.13169.12547.035242_retail_prod_signed.ffu")
                    );
                }
            }
        }
    }

    /// The microsoft thing also has a special file api for... some reason.
    ///
    /// This provides stuff to get the file locations from that. This is not specific to any
    /// device model and hence there is no corresponding lumia namespace to this one like with
    /// [`package`]
    pub mod file {
        use std::str::FromStr;

        use chain_trans::Trans;
        use http::{uri::Authority, Uri};
        use serde::Deserialize;
        use smallvec::SmallVec;

        use super::package::response::checksum::FileChecksum;

        pub mod uri {
            use std::str::FromStr;

            use http::Uri;
            use serde::{Deserialize, Serialize};

            #[derive(Serialize, Deserialize, Clone, Hash, Debug)]
            #[repr(transparent)]
            /// Primarily exists to enable the usage of serde/parsing-as-validation without
            /// pulling in the `http_serde` crate
            #[serde(try_from = "String", into = "String")]
            pub struct UriWrapper(pub http::Uri);

            impl TryFrom<String> for UriWrapper {
                type Error = http::Error;

                fn try_from(value: String) -> Result<Self, Self::Error> {
                    Uri::from_str(&value).map(Self).map_err(Into::into)
                }
            }

            impl From<UriWrapper> for String {
                fn from(value: UriWrapper) -> Self {
                    value.0.to_string()
                }
            }

            impl From<UriWrapper> for Uri {
                fn from(value: UriWrapper) -> Self {
                    value.0
                }
            }

            impl From<Uri> for UriWrapper {
                fn from(value: Uri) -> Self {
                    Self(value)
                }
            }
        }

        /// Result of a request produced by
        /// [`crate::swrepository_download::raw::package::SoftwarePackage::file_requests`]
        ///
        /// Note that WPinternals does:
        /// `FileUrl.url.Replace("sr.azureedge.net", "softwarerepo.blob.core.windows.net");`
        ///
        /// We will do the same just in case with the [`FileInfoResponse::no_azure_edge_url`] function.
        #[derive(Deserialize, Clone, Debug, Hash)]
        pub struct FileInfoResponse {
            #[serde(rename = "url")]
            pub url: uri::UriWrapper,

            /// Usually empty
            #[serde(rename = "alternateUrl", default = "Default::default")]
            pub alt_urls: Vec<uri::UriWrapper>,

            /// File size
            ///
            /// u64 for large files
            #[serde(rename = "fileSize")]
            pub file_size: Option<u64>,

            /// Checksums - usually only one
            #[serde(rename = "checksum")]
            pub checksums: SmallVec<[FileChecksum; 3]>,
        }

        impl TryFrom<String> for FileInfoResponse {
            type Error = serde_json::Error;

            fn try_from(value: String) -> Result<Self, Self::Error> {
                serde_json::from_str(&value)
            }
        }

        impl FromStr for FileInfoResponse {
            type Err = serde_json::Error;

            fn from_str(s: &str) -> Result<Self, Self::Err> {
                serde_json::from_str(s)
            }
        }

        impl FileInfoResponse {
            /// WPinternals does:
            /// `FileUrl.url.Replace("sr.azureedge.net", "softwarerepo.blob.core.windows.net");`
            ///
            /// This function performs that operation on the uri, even though it *probably* is no
            /// longer necessary. It does it on the *primary* `url` field and not the alternate
            /// urls.
            pub fn no_azure_edge_url(self) -> Self {
                let FileInfoResponse {
                    url: uri::UriWrapper(original_primary_url),
                    alt_urls,
                    file_size,
                    checksums,
                } = self;
                original_primary_url
                    .into_parts()
                    .trans_mut(|s| {
                        // Note that while just modifying the host would be preferred,
                        // reconstructing an authority from host and port has no defined method for
                        // it, so instead... we do a string replace on the whole authority and
                        // assume correctness (i.e. panic on fail).
                        if let Some(inner_authority) = s.authority.as_mut() {
                            *inner_authority = inner_authority
                                .as_str()
                                .replace("sr.azureedge.net", "softwarerepo.blob.core.windows.net")
                                .trans(Authority::try_from)
                                .expect("Nothing here should invalidate URI authorities")
                        }
                    })
                    .trans(Uri::from_parts)
                    .expect("valid authority had chunk replaced by hopefully other valid authority")
                    .trans(uri::UriWrapper)
                    .trans(|url| Self {
                        url,
                        alt_urls,
                        file_size,
                        checksums,
                    })
            }
        }
    }
}

/// Lumia product types and such.
pub mod lumia {

    use std::{fmt::Display, str::FromStr};

    use chain_trans::prelude::*;
    use serde::{Deserialize, Serialize};

    #[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
    /// Similar to the various ProductType strings in WPinternals,
    /// this attempts to classify the product type.
    ///
    /// These product types are very fuzzy. Sometimes they have stuff glued on
    /// the end (especially in response to API requests), etc.
    ///
    /// This does several things:
    /// * When converting *from* strings it will normalise any `RMxxxxx` product codes
    ///  to "RM-xxxxxx" product codes, and then classify them, storing the string if it has extra
    ///  data beyond "RMxxxx" product codes.
    /// * When converting TO strings it will provide the normalised value except when the internal
    ///   value is Some(), in which case it will provide that instead.
    ///
    /// Commandline to extract RM-prefixed product codes in WPinternals project:
    /// ```bash
    /// ag '[^a-zA-Z](RM[a-zA-Z0-9-_]+)' -o | cut -d ':' -f 3 | sed 's/.\(.*\)/\1/' | sort | uniq
    /// ```
    ///
    /// REMEMBER TO UPDATE THE AUTHORITATIVE LIST IN [`ProductType::list_known_types`] WHEN
    /// UPDATING
    #[serde(from = "String", into = "String")]
    pub enum ProductType {
        Rm821(Option<String>),
        Rm976(Option<String>),
        Rm1045(Option<String>),
        Rm1072(Option<String>),
        Rm1073(Option<String>),
        Rm1085(Option<String>),
        Rm1113(Option<String>),
        Rm1127(Option<String>),
        Rm1151(Option<String>),
        Rm1152(Option<String>),

        Unknown(String),
    }

    /// Provides a user-friendly display for [`ProductType`]
    pub struct PrettyProductType<'a>(&'a ProductType);

    impl<'a> Display for PrettyProductType<'a> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            let stripped = self.0.get_stripped_identifier();
            let with_extra = self.0.get_extra();
            if let Some(extra) = with_extra {
                f.write_fmt(format_args!("{stripped} (full: {extra})"))
            } else {
                f.write_fmt(format_args!("{stripped}"))
            }
        }
    }

    impl ProductType {
        /// By default, [`ProductType`] handles cases where product strings have extra details in
        /// them - the strings being preserved when extra info is present - and this function
        /// removes that extra data.
        ///
        /// This removes all extra details, effectively leaving this a value-less enum, except that
        /// the data for [`ProductType::Unknown`] values is not erased because that variant carries
        /// no information on its own anyway. That value, unfortunately, ends up being cloned
        pub fn delete_actual_product_strings(&self) -> Self {
            match self {
                ProductType::Rm821(_) => Self::Rm821(None),
                ProductType::Rm976(_) => Self::Rm976(None),
                ProductType::Rm1045(_) => Self::Rm1045(None),
                ProductType::Rm1072(_) => Self::Rm1072(None),
                ProductType::Rm1073(_) => Self::Rm1073(None),
                ProductType::Rm1085(_) => Self::Rm1085(None),
                ProductType::Rm1113(_) => Self::Rm1113(None),
                ProductType::Rm1127(_) => Self::Rm1127(None),
                ProductType::Rm1151(_) => Self::Rm1151(None),
                ProductType::Rm1152(_) => Self::Rm1152(None),
                ProductType::Unknown(v) => Self::Unknown(v.clone()),
            }
        }

        /// Log a warning for an unknown product type.
        ///
        /// Logs info for known ones
        pub fn log_unknown_warning_or_info(&self) {
            use log::*;
            match self {
                ProductType::Unknown(raw_product_type) => {
                    warn!("Unknown product type {raw_product_type}")
                }
                known => {
                    info!(
                        "Product type {} (full: {})",
                        known.delete_actual_product_strings().to_string(),
                        known.to_string()
                    )
                }
            }
        }

        /// Get a list of all known product types.
        ///
        /// These have no special string data.
        pub const fn list_known_types() -> &'static [Self] {
            const V: [ProductType; 10] = [
                ProductType::Rm821(None),
                ProductType::Rm976(None),
                ProductType::Rm1045(None),
                ProductType::Rm1072(None),
                ProductType::Rm1073(None),
                ProductType::Rm1085(None),
                ProductType::Rm1113(None),
                ProductType::Rm1127(None),
                ProductType::Rm1151(None),
                ProductType::Rm1152(None),
            ];

            &V
        }

        /// If this is unknown, then perform an action and then run a function for each known
        /// product type
        pub fn run_if_unknown<R>(
            &self,
            unknown_processor: impl FnOnce(&str, &Self) -> R,
            mut for_each_known: impl FnMut(Self),
        ) -> Option<R> {
            if let Self::Unknown(s) = self {
                let r = unknown_processor(s, self);
                for known_variant in Self::list_known_types() {
                    for_each_known(known_variant.clone())
                }
                Some(r)
            } else {
                None
            }
        }

        /// Get if this is a known product type
        pub fn is_known(&self) -> bool {
            !matches!(self, Self::Unknown(_))
        }

        /// Run the given function with the value of self as argument if this is known
        ///
        /// For the opposite, see [`Self::run_if_unknown`]
        pub fn run_if_known<R>(&self, func: impl FnOnce(&Self) -> R) -> Option<R> {
            if self.is_known() {
                Some(func(self))
            } else {
                None
            }
        }

        /// Produce a pretty-printer for this product type.
        pub fn pretty(&self) -> PrettyProductType<'_> {
            PrettyProductType(self)
        }

        /// Whether or not this has extra string information carried with it.
        ///
        /// Sometimes product identifiers don't carry extra data, but sometimes they have
        /// extra string stuff on the end that provides more specific things.
        ///
        /// When the identifier is unknown, this is always true.
        pub fn has_extra(&self) -> bool {
            match self {
                ProductType::Rm821(v) => v.is_some(),
                ProductType::Rm976(v) => v.is_some(),
                ProductType::Rm1045(v) => v.is_some(),
                ProductType::Rm1072(v) => v.is_some(),
                ProductType::Rm1073(v) => v.is_some(),
                ProductType::Rm1085(v) => v.is_some(),
                ProductType::Rm1113(v) => v.is_some(),
                ProductType::Rm1127(v) => v.is_some(),
                ProductType::Rm1151(v) => v.is_some(),
                ProductType::Rm1152(v) => v.is_some(),
                ProductType::Unknown(_) => true,
            }
        }

        /// Get any extra string identifier data on top of the enum variant
        ///
        /// Always Some() for unknown.
        pub fn get_extra(&self) -> Option<&str> {
            match self {
                ProductType::Rm821(v) => v.as_ref().map(AsRef::as_ref),
                ProductType::Rm976(v) => v.as_ref().map(AsRef::as_ref),
                ProductType::Rm1045(v) => v.as_ref().map(AsRef::as_ref),
                ProductType::Rm1072(v) => v.as_ref().map(AsRef::as_ref),
                ProductType::Rm1073(v) => v.as_ref().map(AsRef::as_ref),
                ProductType::Rm1085(v) => v.as_ref().map(AsRef::as_ref),
                ProductType::Rm1113(v) => v.as_ref().map(AsRef::as_ref),
                ProductType::Rm1127(v) => v.as_ref().map(AsRef::as_ref),
                ProductType::Rm1151(v) => v.as_ref().map(AsRef::as_ref),
                ProductType::Rm1152(v) => v.as_ref().map(AsRef::as_ref),
                ProductType::Unknown(v) => Some(v.as_ref()),
            }
        }

        /// Get the bare string of the variant.
        ///
        /// This is *like* `self.delete_actual_product_strings().to_string()`, except that
        /// in the unknown case it returns `"unknown"` rather than the embedded string identifier.
        ///
        /// Note that this is the AUTHORITATIVE SOURCE if raw/stripped identifier strings.
        pub const fn get_stripped_identifier(&self) -> &'static str {
            match self {
                ProductType::Rm821(_) => "RM-821",
                ProductType::Rm976(_) => "RM-976",
                ProductType::Rm1045(_) => "RM-1045",
                ProductType::Rm1072(_) => "RM-1072",
                ProductType::Rm1073(_) => "RM-1073",
                ProductType::Rm1085(_) => "RM-1085",
                ProductType::Rm1113(_) => "RM-1113",
                ProductType::Rm1127(_) => "RM-1127",
                ProductType::Rm1151(_) => "RM-1151",
                ProductType::Rm1152(_) => "RM-1152",
                ProductType::Unknown(_) => "unknown",
            }
        }
    }

    #[cfg(test)]
    /// This module essentially constructs a match statement on the enum then checks that
    /// [`ProductType::list_known_types`]'s result contains each variant (other than unknown).
    mod assert_product_type_list_contains_all {
        use chain_trans::Trans;

        use super::ProductType;

        #[test]
        fn match_check() {
            fn assert_contains(variant: impl Fn(Option<String>) -> ProductType) {
                assert!(ProductType::list_known_types().contains(&variant(None)));
            }

            // Arbitrary value to match on that is used to check matches.
            //
            // Note that unfortunately we can't match on every variant to check containment.
            match ProductType::Rm821(None) {
                ProductType::Rm821(_) => ProductType::Rm821.trans(assert_contains),
                ProductType::Rm976(_) => ProductType::Rm976.trans(assert_contains),
                ProductType::Rm1045(_) => ProductType::Rm1045.trans(assert_contains),
                ProductType::Rm1072(_) => ProductType::Rm1072.trans(assert_contains),
                ProductType::Rm1073(_) => ProductType::Rm1073.trans(assert_contains),
                ProductType::Rm1085(_) => ProductType::Rm1085.trans(assert_contains),
                ProductType::Rm1113(_) => ProductType::Rm1113.trans(assert_contains),
                ProductType::Rm1127(_) => ProductType::Rm1127.trans(assert_contains),
                ProductType::Rm1151(_) => ProductType::Rm1151.trans(assert_contains),
                ProductType::Rm1152(_) => ProductType::Rm1152.trans(assert_contains),
                // Ignore because this is only for known types.
                ProductType::Unknown(_) => {}
            }
        }
    }

    impl From<String> for ProductType {
        fn from(value: String) -> Self {
            ProductType::from(value.as_str())
        }
    }

    impl FromStr for ProductType {
        type Err = std::convert::Infallible;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            Self::from(s).trans(Ok)
        }
    }

    impl<'a> From<&'a str> for ProductType {
        fn from(value: &'a str) -> Self {
            // Why use or_else and not a macro?
            //
            // The reason is that RM- and non-RM product models require different match
            // statement patterns, and macros can't generate only some parts of a match statement
            //
            // Therefore instead we do the following architecture:
            // * Have an Option<Self> representing the final result
            // * have higher-order functions that take parameters and produces a selection function
            //   that only produces Some(Self) if it matches.
            // * Have a finalisation method that fills in the default value.
            // * use `Option::or` to chain them together in a massive 'match-equivalency
            // statement', though because of limitations on impl Fn -> impl Fn -> ... it isn't
            // quite as lazy as I would like

            /// Normalise RMxxxx to RM-xxxxx in a case where we don't match on the raw string
            /// ref.
            fn normalise_rm_prefix(s: String) -> Option<String> {
                if s.starts_with("RM") && !s.starts_with("RM-") {
                    Some(String::from("RM-").trans_mut(|r| r.push_str(&s[2..])))
                } else {
                    Some(s)
                }
            }

            /// Match on a model that has an RM prefix, normalising as appropriate.
            ///
            /// This automatically determines the with-dash prefix from the provided variant
            /// function.
            fn rm_model<'a>(
                without_dash: &'a str,
                mut enum_variant: impl FnMut(Option<String>) -> ProductType + 'a,
            ) -> impl 'a + FnOnce(&str) -> Option<ProductType> {
                move |match_on| match match_on {
                    v if v == enum_variant(None).get_stripped_identifier() || v == without_dash => {
                        Some(enum_variant(None))
                    }
                    v if v.starts_with(enum_variant(None).get_stripped_identifier())
                        || v.starts_with(without_dash) =>
                    {
                        v.to_owned()
                            .trans(normalise_rm_prefix)
                            .trans(enum_variant)
                            .trans(Some)
                    }
                    _ => None,
                }
            }

            macro_rules! or_else_chain {
                ($value:ident, $($expr:expr),* $(,)?) => {
                    None$(.or(($expr)($value)))*
                }
            }

            or_else_chain! {
                value,
                rm_model("RM821", Self::Rm821),
                rm_model("RM976", Self::Rm976),
                rm_model("RM1045", Self::Rm1045),
                rm_model("RM1072", Self::Rm1072),
                rm_model("RM1073", Self::Rm1073),
                rm_model("RM1085", Self::Rm1085),
                rm_model("RM1113", Self::Rm1113),
                rm_model("RM1127", Self::Rm1127),
                rm_model("RM1151", Self::Rm1151),
                rm_model("RM1152", Self::Rm1152)
            }
            .unwrap_or_else(|| Self::Unknown(value.to_owned()))
        }
    }

    impl From<ProductType> for String {
        fn from(value: ProductType) -> Self {
            <&str>::from(&value).to_owned()
        }
    }

    impl<'a> From<&'a ProductType> for &'a str {
        fn from(value: &'a ProductType) -> Self {
            match value {
                ProductType::Unknown(product_type_raw_string) => product_type_raw_string.as_str(),
                known => known
                    .get_extra()
                    .unwrap_or_else(|| known.get_stripped_identifier()),
            }
        }
    }

    impl ToString for ProductType {
        fn to_string(&self) -> String {
            <&Self as Into<&str>>::into(self).to_owned()
        }
    }

    impl<'a> ToString for &'a ProductType {
        fn to_string(&self) -> String {
            <&str as From<Self>>::from(self).to_string()
        }
    }

    #[cfg(test)]
    mod product_type_ser_deser_tests {
        use serde::Deserialize;
        use serde_json::json;

        use crate::swrepository_download::lumia::ProductType;

        #[test]
        /// Check that any normal case will avoid bundling extra string data, and will transform
        /// missing dashes appropriately
        pub fn no_unnecessary_string_bundles() {
            let pt_in = ProductType::from("RM-1072");
            let pt_in2 = ProductType::from("RM1072");
            let pt_in3 = ProductType::from("RM-1072_de");
            // Same postfix except missing -, should get normalised to same as pt_in3
            let pt_in4 = ProductType::from("RM1072_de");

            assert_eq!(pt_in, pt_in2);
            assert_eq!(pt_in3, pt_in4);

            assert_eq!(
                pt_in.delete_actual_product_strings(),
                pt_in3.delete_actual_product_strings()
            );
        }

        #[test]
        pub fn ser_de_self_consistency() {
            let i = json!("RM-821");
            let v = ProductType::deserialize(i.clone()).expect("Should come from strings");
            assert_eq!(serde_json::to_value(v).unwrap(), i)
        }
    }

    /// Package discovery query construction - see [`super::raw::package::PackageAPIJson`].
    ///
    /// Note that this is kind of a mess - the APIs and generality will probably be refined over
    /// time.
    pub mod package {
        pub mod requests {
            use serde::Serialize;

            use crate::swrepository_download::{
                lumia::ProductType,
                raw::package::{DiscoveryCondition, PackageQueryJson},
            };

            use crate::swrepository_download::raw::package::PackageType;

            /// Indicates the package type (firmware, test mode, etc.) for a given query
            pub trait DiscoveryQueryPackageTyped {
                fn package_type(&self) -> PackageType;
            }

            /// Extension methods for constructing [`FullLumiaDiscoveryQuery`] automatically
            pub trait DiscoveryQueryPackageTypedExt:
                DiscoveryQueryPackageTyped + Serialize
            {
                /// Turn this into a general discovery query
                fn into_discovery_query(self) -> FullLumiaDiscoveryQuery<'static, Self>
                where
                    Self: Sized,
                {
                    FullLumiaDiscoveryQuery::from_query(self)
                }
            }

            impl<T: DiscoveryQueryPackageTyped + Serialize> DiscoveryQueryPackageTypedExt for T {}

            #[derive(Serialize, Clone, Debug)]
            /// a full search query that provides the necessary defaults for things like manufacturer
            /// and product line fields.
            pub struct FullLumiaDiscoveryQuery<
                'a,
                InnerQuery: Serialize + DiscoveryQueryPackageTyped,
            > {
                #[serde(rename = "manufacturerName")]
                manufacturer: &'a str,
                #[serde(rename = "manufacturerProductLine")]
                product_line: &'a str,
                #[serde(rename = "packageType")]
                package_type: PackageType,
                #[serde(rename = "packageClass")]
                /// We set this always to be "Public". Actual package classes seem to vary and indicate
                /// intended usage, I think? I've seen at least 'Factory', 'Testing', 'Public' and some
                /// more such things.
                package_class: &'a str,
                #[serde(flatten)]
                actual_query: InnerQuery,
            }

            impl<'a, InnerQuery: Serialize + DiscoveryQueryPackageTyped>
                FullLumiaDiscoveryQuery<'a, InnerQuery>
            {
                pub fn from_query(q: InnerQuery) -> Self {
                    Self {
                        manufacturer: "Microsoft",
                        product_line: "Lumia",
                        package_type: q.package_type(),
                        package_class: "Public",
                        actual_query: q,
                    }
                }

                pub fn construct_full_api_query(self) -> PackageQueryJson<'static, Self> {
                    PackageQueryJson::with_query(self)
                }

                pub fn construct_full_api_query_with_condition(
                    self,
                    condition: DiscoveryCondition,
                ) -> PackageQueryJson<'static, Self> {
                    PackageQueryJson::with_query_and_condition(self, condition)
                }
            }

            #[derive(Serialize, Clone, Debug)]
            /// Search for firmware for the given type of product
            pub struct ProductTypeFirmwareQuery {
                #[serde(rename = "manufacturerHardwareModel")]
                pub product_type: ProductType,
            }

            impl DiscoveryQueryPackageTyped for ProductTypeFirmwareQuery {
                fn package_type(&self) -> PackageType {
                    PackageType::Firmware
                }
            }
        }
    }
}

//  rswp
//  Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
