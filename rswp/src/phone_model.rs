//! Module for the construction of messages to Nokia Lumia models.
//!
//! Recommended usage is via [`methods`] submodule, which provides known means to construct
//! messages for RPC on nokia phones, extracted from `WPinternals` code.
use serde::{de::DeserializeOwned, Serialize};

/// Used internally to add "MessageVersion": 0 parameter to the message
#[derive(Serialize)]
struct WithMessageVersion<InnerMessage: Serialize> {
    #[serde(flatten)]
    message: InnerMessage,
    #[serde(rename = "MessageVersion")]
    message_version: usize,
}

impl<T: Serialize> From<T> for WithMessageVersion<T> {
    fn from(value: T) -> Self {
        Self {
            message: value,
            message_version: 0,
        }
    }
}

#[cfg(test)]
mod message_version_tests {
    use serde::Serialize;
    use serde_json::json;

    use super::WithMessageVersion;

    #[derive(Serialize)]
    struct Testy<'a> {
        pub name_1: &'a str,
        pub special_nums: &'a [u8],
    }

    #[test]
    pub fn byte_list_is_ints() {
        assert_eq!(
            serde_json::to_value(Testy {
                name_1: "fancy_name",
                special_nums: &[5u8, 3, 9, 4]
            })
            .expect("Should at least be valid"),
            json!({
                "name_1": "fancy_name",
                "special_nums": [5, 3, 9, 4]
            })
        )
    }

    #[test]
    pub fn message_version_works() {
        assert_eq!(
            serde_json::to_value(WithMessageVersion::from(Testy {
                name_1: "extra-fancy-fancy :)",
                special_nums: &[0u8, 1, 4, 6]
            }))
            .expect("should be valid json"),
            json!({
                "name_1": "extra-fancy-fancy :)",
                "special_nums": [0, 1, 4, 6],
                "MessageVersion": 0
            })
        )
    }

    #[derive(Serialize)]
    pub struct Conflicting {
        #[serde(rename = "MessageVersion")]
        pub mv: usize,
    }

    #[test]
    pub fn message_version_name_conflict() {
        assert_eq!(
            serde_json::to_value(WithMessageVersion::from(Conflicting { mv: 4 })).unwrap(),
            json!({
                "MessageVersion": 0
            })
        )
    }

    #[test]
    pub fn empty_params() {
        #[derive(Serialize)]
        struct Testy2<T> {
            pub value: T,
            pub ext: usize,
        }

        assert_eq!(serde_json::to_value(()).unwrap(), json!(null));
        assert_eq!(
            serde_json::to_value(Testy2 { value: (), ext: 4 }).unwrap(),
            json!({
                "ext": 4,
                "value": null
            })
        )
    }
}

/// Constant version string used for Nokia phone comms.
pub const JSONRPC_VER_STRING: &str = "2.0";

/// The full method message for the given innermost message, for JSON encoding, as sent to Nokia
/// phones.
#[derive(Serialize)]
pub struct NokiaJsonMethodMessage<'s, InnerMessage: Serialize> {
    /// Json RPC string
    jsonrpc: &'s str,
    /// Message ID - a constantly incrementing value over time
    id: u32,
    /// Method string for Nokia.
    method: &'s str,
    /// Parameters of the message - note that this automatically wraps with `MessageVersion: 0`
    ///
    /// We NEVER need to worry about this being empty - it wraps so that `MessageVersion: 0` is
    /// always included because it *flattens*. That is to say, we don't need to worry about the c#
    /// vs serde behaviour for "empty dictionaries" because the dictionary is never empty :)
    params: WithMessageVersion<InnerMessage>,
}

impl<'s, InnerMessage: Serialize> NokiaJsonMethodMessage<'s, InnerMessage> {
    pub fn new(message_id: u32, method: &'s str, message_parameters: InnerMessage) -> Self {
        Self {
            jsonrpc: JSONRPC_VER_STRING,
            id: message_id,
            method,
            params: message_parameters.into(),
        }
    }
}

pub trait NokiaJsonMethod {
    /// Name of the method as provided to the Nokia Model API
    const NAME: &'static str;

    /// Name of the property to get from the raw returned JSON.
    ///
    /// Nokia RPC calls return a json object containing a `result` key, which then itself
    /// contains a key that we care about. This specifies that key. See `ResultElement` parameter
    /// in the WPinternals method for some information that it does.
    const RESULT_SUBKEY: &'static str;

    /// Parameters to serialize into JSON
    ///
    /// Note that:
    ///  * byte arrays should be translated into INTEGER ARRAYS - note that serde does this
    ///  correctly just from byte slices so don't worry!
    ///  * "MessageVersion" will be replaced with a zero value.
    type Parameters: Serialize;

    /// The final, strongly typed and meaningful, result of the JSON method.
    ///
    /// Can encode e.g. all known/recognised versions or similar converted from a string.
    type FinalisedResult;

    /// Error that can be produced when attempting to give stronger typing to the original JSON
    /// response.
    type ResponseConversionError;

    /// Attempt to provide structure to the result.
    ///
    /// [`None`] indicates that there was no "result" at all
    /// [`Some(None)`] indicates that there was a "result" key, but it did not contain the
    ///   [`Self::RESULT_SUBKEY`] key.
    ///[`Some(Some(serde_json::Value))`] indicates that the key was actually present.
    fn make_typed(
        resultant_value: Option<Option<serde_json::Value>>,
    ) -> Result<Self::FinalisedResult, Self::ResponseConversionError>;
}

/// Extension trait providing methods from [`NokiaJsonMethod`]
pub trait NokiaJsonMethodExt: NokiaJsonMethod {
    /// Create a message without serializing it to JSON - this requires a message ID, which is a
    /// counter that increments for each connection.
    ///
    /// If you want to get raw JSON, it's better to use
    /// [`NokiaJsonMethodExt::create_json_serialized_message`]
    fn create_unserialized_message(
        message_id: u32,
        message_parameters: Self::Parameters,
    ) -> NokiaJsonMethodMessage<'static, Self::Parameters> {
        NokiaJsonMethodMessage::new(message_id, Self::NAME, message_parameters)
    }

    /// Construct a message serialized into JSON structures to send to a nokia phone - see
    /// [`Self::create_unserialized_message`]. `message_id` is a per-connection, continuously
    /// incrementing integer.
    ///
    /// This assumes that the json produced is valid via [`Result::expect`], which it should be for
    /// known message types unless something has gone really wrong somewhere.
    fn create_json_serialized_message(
        message_id: u32,
        message_parameters: Self::Parameters,
    ) -> serde_json::Value {
        serde_json::to_value(Self::create_unserialized_message(
            message_id,
            message_parameters,
        ))
        .expect("Known message should produce valid Json")
    }
}

impl<T: NokiaJsonMethod> NokiaJsonMethodExt for T {}

/// Nokia JSON methods.
///
/// See `WPinternals/<CommandLine.cs, various others>`
pub mod methods {

    /// Identifying methods.
    pub mod ident {
        use crate::phone_model::NokiaJsonMethod;

        /// Read the product code from a nokia phone
        pub struct ReadProductCode;
        /*
        impl NokiaJsonMethod for ReadProductCode {
            const NAME: &'static str = "ReadProductCode";
            const RESULT_SUBKEY: &'static str = "ProductCode";

            type Parameters = ();

            type FinalisedResult;

            type ResponseConversionError;

            fn make_typed(
                resultant_value: Option<serde_json::Value>,
            ) -> Result<Self::FinalisedResult, Self::ResponseConversionError> {
                todo!()
            }
        }
        */
    }
}

//  rswp
//  Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
