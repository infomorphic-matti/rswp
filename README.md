# rswp
Cross-platform reimplementation of [Windows Phone Internals](https://github.com/ReneLergner/WPinternals) in rust for use by linux folks such as myself. Full credit goes to them, this is essentially reimplementing it as a library and various client tools, because building their program on linux doesn't work effectively.
