//! Modules for downloading, listing, and processing various files

use bpaf::*;
use rswp::swrepository_download::lumia::ProductType;

#[derive(Bpaf, Clone, Hash, Debug)]
/// Filter the filetypes from the list of files returned by the REST api
#[bpaf(fallback(FileTypeFilter::Ffu))]
pub enum FileTypeFilter {
    /// Filter for FFU (firmware update) files
    Ffu,
    /// Test for an arbitrary extension
    Extension(#[bpaf(long("extension"), short('e'))] String),
    /// No filter
    All,
    /// Take just the first file - can be applied on top of a previous filter
    First,
}

#[derive(Bpaf, Clone, Debug)]
/// Filter for the product to retrieve
pub enum SoftwarePackageProductFilter {
    ProductType(#[bpaf(short('p'), long("product-type"))] ProductType),
    /// TODO: Determine if we can replace with an enum + unknown-variant
    ProductCode(#[bpaf(short('c'), long("code"))] String),
    /// Filter based on the operator code and  product type
    ///
    ///
    /// TODO: Determine if we can replace with an enum + unknown variant
    ///
    /// Note that WPinternals never calls with operator code alone, always in combo with
    /// product type
    OperatorAndProductType(
        #[bpaf(long("operator-code"), short('o'), argument)] String,
        #[bpaf(long("product-type"), short('p'), argument)] ProductType,
    ),
}

impl SoftwarePackageProductFilter {
    /// If this has a product type, get it.
    pub fn product_type(&self) -> Option<&ProductType> {
        match self {
            SoftwarePackageProductFilter::ProductType(p) => Some(p),
            SoftwarePackageProductFilter::ProductCode(_) => None,
            SoftwarePackageProductFilter::OperatorAndProductType(_operator_code, p) => Some(p),
        }
    }

    /// Perform logging of information, warnings, etc. for the supplied product filter.
    pub fn do_info_logging(&self) {
        let _ = self.product_type().map(|prod_type| {
            let _ = prod_type.run_if_unknown(
                |unknown_id, _unknown| {
                    log::warn!("Searching has unknown product ID ({unknown_id})");
                    log::info!("Listing known product IDs")
                },
                |known| {
                    let pretty = known.pretty();
                    log::info!("{pretty}");
                },
            );

            let _ = prod_type.run_if_known(|prod_type| {
                let p = prod_type.pretty();
                log::info!("Search has product type: {p}")
            });
        });
    }
}

#[derive(Clone, Debug)]
pub enum PipelineCmd {
    /// Filter the list of files returned by the API when requesting software packages.
    FilterFiles { file_type_filter: FileTypeFilter },
    /// Retrieve file data for the given query
    RetrieveSoftwarePackageFiles {
        product_filter: SoftwarePackageProductFilter,
    },
}

impl PipelineCmd {
    /// Perform informational logging.
    pub fn do_info_logging(&self) {
        match self {
            PipelineCmd::FilterFiles {
                file_type_filter: _,
            } => {}
            PipelineCmd::RetrieveSoftwarePackageFiles { product_filter } => {
                product_filter.do_info_logging();
            }
        }
    }
}

pub fn single_pipeline_command() -> impl Parser<PipelineCmd> {
    fn file_filter_cmd() -> impl Parser<PipelineCmd> {
        let file_type_filter = file_type_filter();
        construct!(PipelineCmd::FilterFiles { file_type_filter })
            .to_options()
            .command("filter")
            .help("Filter the list of files returned from the REST api")
            // Allow chaining commands
            .adjacent()
    }

    fn retrieve_cmd() -> impl Parser<PipelineCmd> {
        let product_filter = software_package_product_filter();
        construct!(PipelineCmd::RetrieveSoftwarePackageFiles {product_filter})
            .to_options()
            .command("packages")
            .help("Retrieve the software packages (and associated file lists) with the given product criteria")
            .adjacent()
    }

    // Select any one of them
    construct!([file_filter_cmd(), retrieve_cmd()])
}

pub fn pipeline_command() -> impl Parser<Vec<PipelineCmd>> {
    single_pipeline_command().many()
        .to_options()
        .command("get")
        .help("Get files from the remote Microsoft repository and filter, process, download, and patch them")
}

//  rswp-cli
//  Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
