//! CLI code

use bpaf::{construct, Bpaf, Parser};

use self::logging::LogLevel;

pub mod get;

pub mod logging {
    use bpaf::Bpaf;
    #[derive(Bpaf, Clone, Copy, PartialEq, Eq, Hash, Debug)]
    #[bpaf(fallback(LogLevel::Info))]
    pub enum LogLevel {
        /// Log to tracing level (very verbose)
        Trace,
        /// Log debug messages (verbose)
        Debug,
        /// Log info messages
        Info,
        /// Only log warnings and errors
        Warn,
        /// Log just errors
        Error,
    }

    /// Set the log level
    impl LogLevel {
        /// Set the logger, or crash if failed.
        pub fn set_logger(self) {
            simple_logger::init_with_level(self.into()).expect("Couldn't set logger???")
        }
    }

    impl Default for LogLevel {
        fn default() -> Self {
            LogLevel::Info
        }
    }

    impl From<LogLevel> for log::LevelFilter {
        fn from(value: LogLevel) -> Self {
            match value {
                LogLevel::Trace => log::LevelFilter::Trace,
                LogLevel::Debug => log::LevelFilter::Debug,
                LogLevel::Info => log::LevelFilter::Info,
                LogLevel::Warn => log::LevelFilter::Warn,
                LogLevel::Error => log::LevelFilter::Error,
            }
        }
    }

    impl From<LogLevel> for log::Level {
        fn from(value: LogLevel) -> Self {
            match value {
                LogLevel::Trace => log::Level::Trace,
                LogLevel::Debug => log::Level::Debug,
                LogLevel::Info => log::Level::Info,
                LogLevel::Warn => log::Level::Warn,
                LogLevel::Error => log::Level::Error,
            }
        }
    }
}

/// Simple license command
pub mod license_cmd {
    use bpaf::Bpaf;

    #[derive(Bpaf, Clone, Hash, Debug)]
    #[bpaf(command, version)]
    /// Print the license of this program
    ///
    /// Not really very interesting, that's all this command does.
    pub struct License();

    impl License {
        pub fn execute(&self) {
            println!(
                r#"rswp
Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"#
            )
        }
    }
}

#[derive(Bpaf, Clone, Debug)]
#[bpaf(options, version)]
/// CLI for the rust sort-of-rewrite of `WPinternals`
///
/// The original WPinternals repository - which this is based on - can be found at:
///  https://github.com/ReneLergner/WPinternals
///
/// This is a CLI rewritten in rust for several reasons, most importantly that you can
/// actually reliably build it on non-Windows systems.
pub struct MainCmd {
    #[bpaf(external(logging::log_level))]
    pub verbosity: LogLevel,
    #[bpaf(external(main_cmds))]
    pub cmd: MainCmds,
}

impl MainCmd {
    pub fn execute(&self) {
        self.verbosity.set_logger();
        self.cmd.execute();
    }
}

#[derive(Clone, Debug)]
pub enum MainCmds {
    License(license_cmd::License),
    Get(Vec<get::PipelineCmd>),
}

impl MainCmds {
    pub fn execute(&self) {
        match self {
            MainCmds::License(v) => v.execute(),
            MainCmds::Get(commands) => {
                for c in commands {
                    c.do_info_logging() 
                }
            }
        }
    }
}

/// Generate the main commands
pub fn main_cmds() -> impl Parser<MainCmds> {
    let license = license_cmd::license().map(MainCmds::License);
    let get = get::pipeline_command().map(MainCmds::Get);

    construct!([license, get])
}

#[cfg(test)]
mod test {
    #[test]
    pub fn assert_command_validity() {
        super::main_cmd().check_invariants(false)
    }
}

//  rswp-cli
//  Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
