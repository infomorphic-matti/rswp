#![cfg_attr(doc, feature(doc_auto_cfg))]
//! CLI that is a modification+rewrite of `WPinternals` (Windows Phone Internals), in Rust.
//!
//! This is to enable the modification of Windows Phones from linux, as the WPinternals project is
//! not easily built, even with `mono` and `msbuild` (in fact, I didn't manage to build it at all).
//!
//!
//! # Cargo (Buildtime) Features
//! This inherits some features from the `bpaf` crate - [docs here](https://docs.rs/bpaf/latest/bpaf/index.html#cargo-features):
//! * `autocomplete`, which adds support for autocompleting to various shells. (enabled by default)
//! * `bright-color`, which makes brighter colours (enabled by default)
//! * `dull-color`, which is like `bright-color` but less bright (disable default features and
//!   enable this to switch from `bright-color`).

pub mod cli;

fn main() {
    // Parse
    let cmd = cli::main_cmd().run();
    // Run
    cmd.execute();
}

//  rswp-cli
//  Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
