#![cfg_attr(doc, feature(doc_auto_cfg))]
//! Library for constructing pipelines dynamically, while ensuring correct types etc.
//!
//! Part of the rswp project as a utility, but should be useable for anyone. I was not well when I
//! made this library so its got a kinda dodgy design.
//!
//! # Architecture
//! This is a library for constructing threading pipelines which are capable of dynamic type
//! validation and dynamic construction.
//!
//! The key trait is [`IntoCommandChain`], which allows building a chain of "commands" that can
//! process many sequences/chunks of input, and produce many sequences/chunks of output. This trait
//! has a type parameter indicating the input type, and an associated parameter indicating the
//! output type.
//!
//! It is often desireable to do things when, for instance, the previous command emitted an error
//! of one kind or another, or an optional value. Because of the architecture of of the
//! [`IntoCommandChain`] trait, this would require duplication of the implementation for each input
//! type with minor variations. To fix this problem, a utility macro for common implementation
//! styles is provided with [`command_chain`].

use anyhow::Error as AHError;
use anyhow::Result as AnyResult;

/// Reexport of anyhow.
pub use anyhow;

use chain_trans::Trans;
use std::{
    any::{Any, TypeId},
    collections::HashSet,
    error::Error,
    marker::PhantomData,
    sync::mpsc::{self, Receiver, Sender},
    thread::{Scope, ScopedJoinHandle},
};

/*
/// Trait that eases the use of [`Any`]-like APIs with vectors of types rather than normal types
///
/// Implemented on any vector with [`Any`] elements.
pub mod any_vec {
    use std::any::{TypeId, Any};

    pub trait AnyVec: Any {
        /// Get the type ID of the elements, like [`Any::type_id`].
        fn element_type_id(&self) -> TypeId;

        /// Make an empty version of the vector of the same type.
        fn make_empty(&self) -> Box<dyn AnyVec>;

        /*
        #[inline]
        fn as_any_ref(&self) -> &dyn Any {
            &self as &dyn Any
        }

        #[inline]
        fn as_any_mut(&mut self) -> &mut dyn Any {
            &mut self as &mut dyn Any
        }
        */
    }

    impl<T: Any> AnyVec for Vec<T> {
        fn element_type_id(&self) -> TypeId {
            TypeId::of::<T>()
        }

        fn make_empty(&self) -> Box<dyn AnyVec> {
            Box::new(Self::default())
        }
    }

    impl dyn AnyVec {
        #[inline]
        pub fn elements_are<T: Any>(&self) -> bool {
            self.element_type_id() == TypeId::of::<T>()
        }

        #[inline]
        pub fn downcast_ref<T: Any>(&self) -> Option<&Vec<T>> {
            self.as_any_ref().downcast_ref::<Vec<T>>()
        }

        #[inline]
        pub fn downcast_mut<T: Any>(&mut self) -> Option<&mut Vec<T>> {
            self.as_any_mut().downcast_mut::<Vec<T>>()
        }
    }

    impl dyn AnyVec + Send {
        #[inline]
        pub fn elements_are<T: Any>(&self) -> bool {
            <dyn AnyVec>::elements_are::<T>(self)
        }

        #[inline]
        pub fn downcast_ref<T: Any>(&self) -> Option<&Vec<T>> {
            <dyn AnyVec>::downcast_ref::<T>(self)
        }

        #[inline]
        pub fn downcast_mut<T: Any>(&mut self) -> Option<&mut Vec<T>> {
            <dyn AnyVec>::downcast_mut::<T>(self)
        }
    }

    impl dyn AnyVec + Send + Sync {
        #[inline]
        pub fn elements_are<T: Any>(&self) -> bool {
            <dyn AnyVec>::elements_are::<T>(self)
        }

        #[inline]
        pub fn downcast_ref<T: Any>(&self) -> Option<&Vec<T>> {
            <dyn AnyVec>::downcast_ref::<T>(self)
        }

        #[inline]
        pub fn downcast_mut<T: Any>(&mut self) -> Option<&mut Vec<T>> {
            <dyn AnyVec>::downcast_mut::<T>(self)
        }
    }
}

/// An Ok type erased result using anyhow errors for the other end.
pub mod any_result {
    use std::any::{Any, TypeId};

    pub trait AnyResult {
        /// Get the type ID of the OK element if it would exist.
        fn ok_type_id(&self) -> TypeId;
    }

    impl<T: Any> AnyResult for Result<T, anyhow::Error> {
        fn ok_type_id(&self) -> TypeId {
            TypeId::of::<T>()
        }
    }
}
*/

#[derive(thiserror::Error, Debug)]
/// Error in construction of a dynamic pipeline.
#[error("Could not construct pipeline - source produces `{mismatch_output:?}`s, but the target accepts `{accepts:?}` as inputs. Error is from command {idx} and next in the pipeline.")]
pub struct PipelineConstructionError {
    /// The output of the source pipeline that mismatches the input of the target pipeline
    pub mismatch_output: TypeId,

    /// The inputs the target pipeline accepts
    pub accepts: HashSet<TypeId>,

    /// Raw pipeline index, used by users to identify the step in the pipeline. This is the index
    /// of the first command in the pipe pair in the overall pipeline.
    ///
    /// [`CommandChain::with_base_index`] and [`CommandChain::get_base_index`] are related to
    /// this.
    pub idx: usize,
}

/// Represents a chain of commands constructed as various threads with message passing
/// architecture.
///
/// These are designed to construct threads from a pair of input and output channels. They may
/// branch and produce lots of inner threads, should it be necessary.
///
/// Creating the channels normally can be difficult. It may be easier with [`make_command_channels`]
pub trait IntoCommandChain<I: Send + 'static> {
    type O: Send + 'static;
    /// Construct a thread that receives collections of inputs of the given type to process in
    /// chunks. It should only finish once no more input can possibly be provided - that is, when
    /// sending to the output, you should expect it to be infallible, and are free to panic if it
    /// is not.
    ///
    /// The fallibility is matched to the fallibility of standard thread spawning with
    /// [`std::thread::Builder::spawn_scoped`]
    fn build_threads<'scope, 'env: 'scope>(
        self,
        scope: &'scope Scope<'scope, 'env>,
        inputs: Receiver<Vec<I>>,
        outputs: Sender<Vec<Self::O>>,
    ) -> std::io::Result<Vec<ScopedJoinHandle<'scope, ()>>>
    where
        Self: 'env;
}

/// Construct the threads for a given command chain, along with appropriate channel types.
pub fn make_command_channels<'scope, 'env: 'scope, I: Send + 'static, O: Send + 'static>(
    scope: &'scope Scope<'scope, 'env>,
    chain: impl IntoCommandChain<I, O = O> + 'env,
) -> std::io::Result<(
    Sender<Vec<I>>,
    Receiver<Vec<O>>,
    Vec<ScopedJoinHandle<'scope, ()>>,
)> {
    let (chain_input, into_chain_recv) = mpsc::channel::<Vec<I>>();
    let (into_chain_send, chain_output) = mpsc::channel::<Vec<O>>();
    let thread_handle_bundle = chain.build_threads(scope, into_chain_recv, into_chain_send)?;
    (chain_input, chain_output, thread_handle_bundle).trans(Ok)
}

impl<I: Send + 'static, O: Send + 'static, F: Send + FnMut(I) -> O> IntoCommandChain<I> for F {
    type O = O;

    fn build_threads<'scope, 'env: 'scope>(
        mut self,
        scope: &'scope Scope<'scope, 'env>,
        inputs: Receiver<Vec<I>>,
        outputs: Sender<Vec<Self::O>>,
    ) -> std::io::Result<Vec<ScopedJoinHandle<'scope, ()>>>
    where
        Self: 'env,
    {
        vec![std::thread::Builder::new().spawn_scoped(scope, move || {
            while let Ok(next_input_collection) = inputs.recv() {
                let result = next_input_collection
                    .into_iter()
                    .map(&mut self)
                    .collect::<Vec<_>>();
                outputs
                    .send(result)
                    .expect("result processor should be infallible until finished with input");
            }
        })?]
        .trans(Ok)
    }
}

macro_rules! define_tuple_chains {
    ($first:ident, $($o_cc:ident => $next_cc:ident),*; $last:ident) => {
        impl <
            I: Send + 'static,
            $first: IntoCommandChain<I>,
            $($next_cc: IntoCommandChain<$o_cc::O>,)*
        > IntoCommandChain<I> for ($($o_cc,)* $last,) {
            type O = $last::O;

            #[allow(non_snake_case)]
            fn build_threads<'scope, 'env: 'scope>(
                self,
                scope: &'scope Scope<'scope, 'env>,
                inputs: Receiver<Vec<I>>,
                outputs: Sender<Vec<Self::O>>,
            ) -> std::io::Result<Vec<ScopedJoinHandle<'scope, ()>>> where Self: 'env {
                let ($($o_cc,)* $last,) = self;
                let thread_handles = [].into_iter();
                $(
                    let (current_output, next_inputs) = mpsc::channel();
                    let thread_handles = thread_handles.chain($o_cc.build_threads(scope, inputs, current_output)?.into_iter());
                    let inputs = next_inputs;
                )*
                let thread_handles = thread_handles.chain($last.build_threads(scope, inputs, outputs)?.into_iter());
                Ok(thread_handles.collect::<Vec<_>>())
            }
        }
    }
}

#[macro_export]
/// Implement [`IntoCommandChain`] for a type, with convenience syntax for creating implementations
/// for [`Result`] and [`Option`] as well.
///
/// This is for simple command chains that work within a `while let Ok(next_input_seq) = inputs.recv() { for i in next_input_seq {} }` loop.
///
/// To use this, see the examples in the test module. The key is that:
/// * You can define arbitrary primary type converting pipes for a type. Generics can be optionally
///   provided, but they must be within `{` and `}` rather than `<` and `>` on the impl part for
///   parsing reasons.
/// * A primary processing block can be provided, which automatically iterates over the input. This
///   processing block does not provide a way to exit the outer loop, which pulls from the receiver
///   of vectors of input because that should not occur on the happy path unless input runs out.
///   
///   Three "parameters" can be provided to this implementation block - the first must be `self`,
///   the second is the name of the variable containing the current input, and the third is the raw
///   sender for output, upon which you should send vectors of output. This sender is expected to
///   never fail, and you can panic if it does.
/// * There are also extra extractors, which allow reusing the implementation of the happy path for
///   certain types like Option and Result, while allowing other behaviour on errors. These can be
///   provided with generics as well, e.g. constraints on the error type, as well as the extractor
///   name having optional parameters - Option needs none, but Result needs an error param.
/// * These extra extractors get three extra parameters. First, they get the raw wrapped input
///   variable name.
///   Second, a label for the inner loop that processes the input batch.
///   Third, the label for the outer loop that receives from the channel, which should not
///   be broken out of unless in extreme circumstances - i.e. cannot accept any more input
///   at all. The "return value" of the expression you provide should be the normal input as
///   provided to the primary definition of the block.
macro_rules! command_chain {
    {
        $(
        $(#[$meta:meta])*
        <$ty:ty> => $out:ty: $(impl {$($impls:tt)*})? for $impl_target:ty => |$self:ident, $curr_input:ident, $output_sender:ident| $implementation_for_type:block
        $($extra_extractor:ident $(($($extra_params:tt)*))? =>  $(impl {$($impls_extra:tt)*})? |$wrapped_input:ident, $inner:lifetime, $receiver:lifetime| $extractor:expr)*
        )*
    } => {
        $(
        $(#[$meta])*
        impl $(<$($impls)*>)?  $crate::IntoCommandChain<$ty> for $impl_target {
            type O = $out;

            fn build_threads<'scope, 'env: 'scope>(
                $self,
                scope: &'scope ::std::thread::Scope<'scope, 'env>,
                inputs: ::std::sync::mpsc::Receiver<::std::vec::Vec<$ty>>,
                $output_sender: ::std::sync::mpsc::Sender<::std::vec::Vec<Self::O>>
            ) -> ::std::io::Result<::std::vec::Vec<::std::thread::ScopedJoinHandle<'scope, ()>>> where Self: 'env {
                ::core::result::Result::Ok(vec![::std::thread::Builder::new().spawn_scoped(scope, move || {
                    while let Ok(next_chunk) = inputs.recv() {
                        for $curr_input in next_chunk.into_iter() {
                            $implementation_for_type
                        }
                    }
                })?])
            }
        }
        $(command_chain!{@extra $extra_extractor  $(($($extra_params)*))?
            <$ty> => $out: $(impl {$($impls_extra)*})? for $impl_target = |$self, $curr_input, $output_sender| $implementation_for_type
            |$wrapped_input, $inner, $receiver| $extractor
        })*
        )*
    };
    {@extra option
        <$ty:ty> => $out:ty: $(impl {$($impls_extra:tt)*})? for $impl_target:ty = |$self:ident, $curr_input:ident, $output_sender:ident| $implementation_for_type:block
        $(#[$meta_extra:meta])* |$wrapped_input:ident, $inner:lifetime, $receiver:lifetime| $extractor:expr
    } => {

        $(#[$meta_extra])*
        impl $(<$($impls_extra)*>)? $crate::IntoCommandChain<::core::option::Option::<$ty>> for $impl_target {
            type O = $out;

            fn build_threads<'scope, 'env: 'scope>(
                $self,
                scope: &'scope ::std::thread::Scope<'scope, 'env>,
                inputs: ::std::sync::mpsc::Receiver<::std::vec::Vec<::core::option::Option::<$ty>>>,
                $output_sender: ::std::sync::mpsc::Sender<::std::vec::Vec<Self::O>>
            ) -> ::std::io::Result<::std::vec::Vec<::std::thread::ScopedJoinHandle<'scope, ()>>> where Self: 'env {
                ::core::result::Result::Ok(vec![::std::thread::Builder::new().spawn_scoped(scope, move || {
                    $receiver: while let Ok(next_chunk) = inputs.recv() {
                        $inner: for $wrapped_input in next_chunk.into_iter() {
                            let $curr_input = $extractor;
                            $implementation_for_type
                        }
                    }
                })?])
            }
        }
    };

    {@extra result(error: $err_ty:ty)
        <$ty:ty> => $out:ty:  $(impl {$($impls_extra:tt)*})? for $impl_target:ty = |$self:ident, $curr_input:ident, $output_sender:ident| $implementation_for_type:block
        $(#[$meta_extra:meta])* |$wrapped_input:ident, $inner:lifetime, $receiver:lifetime| $extractor:expr
    } => {

        $(#[$meta_extra])*
        impl $(<$($impls_extra)*>)?  $crate::IntoCommandChain<::core::result::Result::<$ty, $err_ty>> for $impl_target {
            type O = $out;

            fn build_threads<'scope, 'env: 'scope>(
                $self,
                scope: &'scope ::std::thread::Scope<'scope, 'env>,
                inputs: ::std::sync::mpsc::Receiver<::std::vec::Vec<::core::result::Result<$ty, E>>>,
                $output_sender: ::std::sync::mpsc::Sender<::std::vec::Vec<Self::O>>
            ) -> ::std::io::Result<::std::vec::Vec<::std::thread::ScopedJoinHandle<'scope, ()>>> where Self: 'env {
                ::core::result::Result::Ok(vec![::std::thread::Builder::new().spawn_scoped(scope, move || {
                    $receiver: while let Ok(next_chunk) = inputs.recv() {
                        $inner: for $wrapped_input in next_chunk.into_iter() {
                            let $curr_input = $extractor;
                            $implementation_for_type
                        }
                    }
                })?])
            }
        }
    }
}

#[cfg(test)]
mod test {
    use std::sync::mpsc;

    use crate::{make_command_channels, IntoCommandChain};

    struct StringLength;

    command_chain! {
        /// Make a big old command thingy
        <String> => usize: for StringLength => |self, i, sender| {
            sender.send(vec![i.len()]).expect("next command must continuously take input");
        }
        // Warn errors and then skip
        result(error: E) => impl {E: Send + 'static + core::fmt::Display} |res, 'curr_batch, 'oh_dear| match res {
            Ok(v) => v,
            Err(e) => { log::warn!("Found error: {e}"); continue 'curr_batch }
        }
        // Skip Nones entirely.
        option => |opt, 'cur, 'overall| match opt {
            Some(v) => v,
            None => continue 'cur
        }
    }

    #[test]
    fn simple_command_impl() {
        std::thread::scope(|s| {
            let (in_chan, out_chan, _handles) =
                make_command_channels::<String, usize>(s, StringLength).unwrap();

            let string: String = "hello im a very happy string :)".into();

            in_chan.send(vec![string.clone()]).unwrap();
            let actually_returned_length = out_chan.recv().unwrap()[0];
            assert_eq!(string.len(), actually_returned_length);
        });
    }
}

define_tuple_chains! {CC0, CC0 => CC1; CC1}
define_tuple_chains! {CC0, CC0 => CC1, CC1 => CC2; CC2}
define_tuple_chains! {CC0, CC0 => CC1, CC1 => CC2, CC2 => CC3; CC3}
define_tuple_chains! {CC0, CC0 => CC1, CC1 => CC2, CC2 => CC3, CC3 => CC4; CC4}
define_tuple_chains! {CC0, CC0 => CC1, CC1 => CC2, CC2 => CC3, CC3 => CC4, CC4 => CC5; CC5}
define_tuple_chains! {CC0, CC0 => CC1, CC1 => CC2, CC2 => CC3, CC3 => CC4, CC4 => CC5, CC5 => CC6; CC6}
define_tuple_chains! {CC0, CC0 => CC1, CC1 => CC2, CC2 => CC3, CC3 => CC4, CC4 => CC5, CC5 => CC6, CC6 => CC7; CC7}
define_tuple_chains! {CC0, CC0 => CC1, CC1 => CC2, CC2 => CC3, CC3 => CC4, CC4 => CC5, CC5 => CC6, CC6 => CC7, CC7 => CC8; CC8}
define_tuple_chains! {CC0, CC0 => CC1, CC1 => CC2, CC2 => CC3, CC3 => CC4, CC4 => CC5, CC5 => CC6, CC6 => CC7, CC7 => CC8, CC8 => CC9; CC9}
define_tuple_chains! {CC0, CC0 => CC1, CC1 => CC2, CC2 => CC3, CC3 => CC4, CC4 => CC5, CC5 => CC6, CC6 => CC7, CC7 => CC8, CC8 => CC9, CC9 => CC10; CC10}
define_tuple_chains! {CC0, CC0 => CC1, CC1 => CC2, CC2 => CC3, CC3 => CC4, CC4 => CC5, CC5 => CC6, CC6 => CC7, CC7 => CC8, CC8 => CC9, CC9 => CC10, CC10 => CC11; CC11}
define_tuple_chains! {CC0, CC0 => CC1, CC1 => CC2, CC2 => CC3, CC3 => CC4, CC4 => CC5, CC5 => CC6, CC6 => CC7, CC7 => CC8, CC8 => CC9, CC9 => CC10, CC10 => CC11, CC11 => CC12; CC12}
define_tuple_chains! {CC0, CC0 => CC1, CC1 => CC2, CC2 => CC3, CC3 => CC4, CC4 => CC5, CC5 => CC6, CC6 => CC7, CC7 => CC8, CC8 => CC9, CC9 => CC10, CC10 => CC11, CC11 => CC12, CC12 => CC13; CC13}
define_tuple_chains! {CC0, CC0 => CC1, CC1 => CC2, CC2 => CC3, CC3 => CC4, CC4 => CC5, CC5 => CC6, CC6 => CC7, CC7 => CC8, CC8 => CC9, CC9 => CC10, CC10 => CC11, CC11 => CC12, CC12 => CC13, CC13 => CC14; CC14}

pub mod prelude {}

//  dynpipeline
//  Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
